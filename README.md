# Gitlab docker build container for Eskild Hustvedt

This repository contains the Dockerfile for a docker image I use for many of my
various gitlab repositories.

It's based upon the node upstream Debian image, then various useful libraries
and utilites are added, like a functional perl installation.

## Variants (tags)

#### :bookworm

Debian 12 base image.

#### :devenv

Derivative of the `bookworm`-image that also contains vim, dependencies for
chrome (but not chrome itself, for use with puppeteer) and dotfiles from
https://gitlab.com/zerodogg/dotfiles

### :tinynode

A tiny node.js image based upon node:lts-alpine. Only contains base image, plus
a few select packages. Useful for CI builds that don't need that much.

### (untagged)

The latest Debian release as of buildtime, currently the same as `:bookworm`.
It's generally better to specify a variant, otherwise things will break once
a new Debian release is out.

#### :bullseye

Debian 11 base image. Currently maintained as a legacy image, only updated once
a month.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/gpl-3.0.txt.
